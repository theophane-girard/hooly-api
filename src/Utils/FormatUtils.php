<?php

namespace App\Utils;

use Symfony\Component\Serializer\SerializerInterface;

class FormatUtils
{

  public function __construct(private SerializerInterface $serializer)
  {  }

  public function formatObjectToArray($data)
  {
    return json_decode($this->serializer->serialize($data, 'json', [
      'ignored_attributes' => ['__initializer__','__cloner__','__isInitialized__']
    ]), true);
  }

}