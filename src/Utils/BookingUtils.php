<?php

namespace App\Utils;

use App\Entity\Booking;
use App\Entity\User;
use App\Repository\BookingRepository;
use DateTime;

class BookingUtils
{
  public function __construct(
    private BookingRepository $bookingRepository
  ) {}

  public function getBookingsLeft(DateTime $date): int
  {
    $bookings = $this->bookingRepository->findBy(['date' => $date]);
    $maxBookings = $date->format('D') === 'Fri' ? 6 : 7;

    return $maxBookings - count($bookings);
  }

  public function isOnceAWeek(User $user, Booking $booking): int
  {
    $dateTime = $booking->getDate();
    $bookingStartWeek = clone $dateTime->modify(('Sunday' == $dateTime->format('l')) ? 'Monday last week' : 'Monday this week');
    $bookingEndWeek = clone $dateTime->modify('Sunday this week');
    $bookings = $this->bookingRepository->findByDateRange(
      $user->getId(),
      $bookingStartWeek,
      $bookingEndWeek
    );

    return count($bookings) === 0;
  }

  public function isValidBookingRequestDate(Booking $booking): int
  {
    $bookingDate = $booking->getDate();
    $today = new \DateTime();

    if ($bookingDate <= $today) {
      return false;
    }

    return true;
  }
}