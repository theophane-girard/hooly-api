<?php

namespace App\Controller;

use App\Factory\BookingFactory;
use App\Repository\BookingRepository;
use App\Repository\UserRepository;
use App\Utils\BookingUtils;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Routing\Annotation\Route;

class BookingController extends AbstractController
{

  public function __construct()
  {
  }

  /**
   * @Route("/bookings", name="book", methods={"POST"})
   */
  public function book(
    Request $request,
    EntityManagerInterface $em,
    UserRepository $userRepository,
    BookingUtils $bookingUtils
  ) {
    
    $requestData = json_decode($request->getContent(), true);
    $requestUserId = $requestData['userId'];
    $requestDate = $requestData['date'];


    $user = $userRepository->findOneBy(['id' => $requestUserId]);
    $date = DateTime::createFromFormat('d-m-Y', $requestDate);

    $booking = BookingFactory::createBooking($date, $user);

    if ($bookingUtils->getBookingsLeft($date) <= 0) {
      throw new Exception('Il n\'y a plus de place pour ce jour', 1);
    }

    if (!$bookingUtils->isOnceAWeek($user, $booking)) {
      throw new Exception('Vous avez dejà réservé une place pour cette semaine', 1);
    }

    if (!$bookingUtils->isValidBookingRequestDate($booking)) {
      throw new Exception('Vous ne pouvez pas réserver pour le jour même ou la date demandée est passée', 1);
    }
    $em->persist($booking);
    $em->flush();
    return $this->json($booking);
  }

  /**
   * @Route("/bookings", name="list bookings", methods={"GET"})
   */
  public function index(
    Request $request,
    BookingRepository $bookingRepository
  ) {
    $bookings = $bookingRepository->findBy(['user' => $request->query->get('id')]);
    return $this->json($bookings);
  }
}
