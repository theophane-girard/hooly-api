<?php

namespace App\Controller;

use App\Factory\UserFactory;
use App\Repository\UserRepository;
use App\Utils\FormatUtils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

  public function __construct()
  {
  }

  /**
   * @Route("/login", name="users", methods={"POST"})
   */
  public function login(
    Request $request,
    UserPasswordHasherInterface $passwordHasher,
    FormatUtils $formatUtils,
    EntityManagerInterface $entityManagerInterface,
    UserRepository $userRepository
  ) {
    $requestData = json_decode($request->getContent(), true);
    $existingUser = $userRepository->findOneBy(['username' => $requestData['username']]);

    if ($existingUser !== null) {
      return $this->json($formatUtils->formatObjectToArray($existingUser));
    }

    $user = UserFactory::createSimpleUser($requestData, $passwordHasher);
    $entityManagerInterface->persist($user);
    $entityManagerInterface->flush();
    return $this->json($formatUtils->formatObjectToArray($user));
  }


}
