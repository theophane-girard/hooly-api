<?php

namespace App\Factory;

use App\Entity\Booking;
use App\Entity\User;
use DateTime;

class BookingFactory
{
  public static function createBooking(DateTime $date, User $user): Booking
  {
    $booking = new Booking();
    $booking->setDate($date);
    $booking->setUser($user);
    return $booking;
  }
}