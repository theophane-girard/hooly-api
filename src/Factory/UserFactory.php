<?php

namespace App\Factory;

use App\Entity\User;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFactory
{
  public static function createSimpleUser(array $request, $passwordHasher): User
  {
    $user = new User();
    $hashedPassword = $passwordHasher->hashPassword(
      $user,
      $request['password']
    );
    $user->setPassword($hashedPassword);
    $user->setUsername($request['username']);
    $user->setRoles(['ROLE_USER']);
    return $user;
  }
}
